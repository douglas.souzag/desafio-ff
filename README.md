[![PHP](https://img.shields.io/badge/php-v7.4-<COLOR>.svg)](https://shields.io/)

# [Landing Page para divulgação de uma Smart TV Samsung]

Este pequeno projeto foi desenvolvido como desafio de entrevista para a empresa [Flip Flop Lab.](https://www.flipfloplab.com.br/)             

## Preview

![Landing Page Preview](https://i.imgur.com/d1GRAC0.png)

## Download e Instalação


* Baixe ou clone o projeto.
* Coloque a pasta dentro de seu servidor apache na pasta htdocs.

## Configuração do Banco

Primeiro a criação da tabela, repare que "***" é o nome do schema onde a tabela será criada.
```sql
CREATE TABLE `***`.`banco_ofertas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(320) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
```
Agora na pasta do projeto, entre no arquivo 'conexao.php' que se localiza em: /php/conexao.php, você deve se deparar com isso:

```php
<?php
//Configurações do banco
$host = "localhost"; 
$username = "root";
$password = "root";
$db = "banco_ofertas";
$schema = "***";

$conn = mysqli_connect($host, $username, $password )or die();  
mysqli_select_db($conn, $schema);  

?>
```
* $schema deve receber o mesmo valor que foi colocado na criação da tabela.
* $username deve receber um usuario que tenha acesso e direitos na database onde a tabela foi criada.
* $password deve receber a senha desse usuario.
* $host deve receber o endereço do servidor.

# ATENÇÃO

**Nos arquivos 'indexController.js' e 'painelController.js' localizados em /js/ existe uma variável 'url' que aponta para o servidor local + pasta do projeto, se o servidor não for local e a pasta do projeto for renomeada, essa variável também precisará ser ajustada conforme a mudança.**

Pronto, agora é só acessar o endereço (normalmente em http://localhost/desafio-ff/index.html, se o servidor for local)

Para acessar o painel de emails cadastrados, basta clicar no botão no canto direito inferior.

## Tecnologias Usadas

 * PHP - para interações com o banco de dados em backend;
 * Html, Css, Bootstrap - para o frontend;
 * MySQL - para o banco;
 * JQuery, AJAX, Javascript - para interação com o backend;