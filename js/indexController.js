$(document).ready(function() {
    $("p.msg").hide();
    $('#verificar-email').click(function(){
      //Desabilitando o submit do form
      $("form").submit(function () { return false; });
      //Atribuindo o valor do campo
      var email	= $("#email").val();
      //Filtros
      var emailFilter=/^.+@.+\..{2,}$/;
      var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/;
      //Filtrando
      if(!(emailFilter.test(email))||email.match(illegalChars)){
        $("p.msg").show().removeClass("ok").addClass("erro")
        .text('Por favor, informe um email válido.');
      }else{
        $.ajax({        
            url:"http://localhost/desafio-ff/php/cadastrar.php",            
            type:"post",                
            data: "email="+email,   
            success: function (result){ 
                if (result == 1) {                      
                    $("p.msg").show().addClass("ok")
                    .text('Seu email foi enviado para o nosso banco de ofertas!');
                } else {
                    $("p.msg").show().addClass("ok")
                    .text('Seu email já esta em nosso banco de ofertas!');
                }
            }
        })
      }
    });
    $('#email').focus(function(){
      $("p.msg.erro").hide();
    });
});
function mudarPagina(){
  window.location.href = "painel.html";
} 